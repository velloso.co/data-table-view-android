package co.velloso.android.datatableview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Checkable
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.data_table.view.*
import kotlinx.android.synthetic.main.data_table_header_column.view.*


class DataTableView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var hasCheckColumn: Boolean = false

    var hasFooter: Boolean = false

    var columns: List<Column> = listOf()
        set(value) {
            field = value
            val inflater = LayoutInflater.from(context)
            value.forEach { column ->
                val view = inflater.inflate(R.layout.data_table_header_column, dataTableHeaderColumns, false)
                view.layoutParams = LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, column.weight)
                view.titleTextView.text = column.title
                view.sortToggleButton.isEnabled = column.value != null
                view.sortToggleButton.isChecked = false
                view.sortToggleButton.setOnCheckedChangeListener { _, isChecked ->
                    column.asc = isChecked
                    onColumnSorted?.invoke(columns)
                }
                dataTableHeaderColumns.addView(view)
            }
        }

    var onColumnSorted: ((columns: List<Column>) -> Unit)? = null

    var onCheckChanged: ((checked: Boolean) -> Unit)? = null

    val checkedItems: List<Checkable>
        get() = (adapter?.currentList as List<Checkable>).filter { it.isChecked }

    var adapter: PagedListAdapter<Checkable, *>?
        get() = listRecyclerView.adapter as? PagedListAdapter<Checkable, *>
        set(value) {
            listRecyclerView.adapter = value
        }

    init {

        inflate(context, R.layout.data_table, this)

        val a = context.obtainStyledAttributes(attrs, R.styleable.DataTableView, 0, 0)

        hasCheckColumn = a.getBoolean(R.styleable.DataTableView_hasCheckColumn, false)
        hasFooter = a.getBoolean(R.styleable.DataTableView_hasFooter, false)

        a.recycle()

        listRecyclerView.layoutManager = LinearLayoutManager(context)
        dataTableHeaderCheckboxLayout.visibility = if (hasCheckColumn) View.VISIBLE
        else View.GONE
        dataTableHeaderCheckbox.setOnCheckedChangeListener { _, isChecked ->
            onCheckChanged?.invoke(isChecked)
        }

        dataTableFooter.visibility = if (hasFooter) View.VISIBLE
        else View.GONE
    }

    data class Column(
            val id: Long,
            val title: String,
            val weight: Float,
            val sortable: Boolean = false,
            var asc: Boolean = true,
            val value: Any? = null
    )
}