package co.velloso.android.datatableview

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.filter_item.view.*
import kotlinx.android.synthetic.main.filter_list.view.*


class FilterListView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    var onFilterRemoved: ((filter: Filter) -> Unit)? = null

    var adapter: PagedListAdapter<Filter, *>?
        get() = filtersRecyclerView.adapter as? PagedListAdapter<Filter, *>
        set(value) {
            filtersRecyclerView.adapter = value
        }

    init {

        inflate(context, R.layout.filter_list, this)

        val a = context.obtainStyledAttributes(attrs, R.styleable.FilterListView, 0, 0)

        filtersEmptyView.text = a.getString(R.styleable.FilterListView_emptyText)

        a.recycle()

        filtersRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        filtersRecyclerView.adapter = FilterAdapter { filter ->
            onFilterRemoved?.invoke(filter)
        }
    }

    fun submitList(list: List<Filter>) {
        if (list.isEmpty()) {
            filtersRecyclerView.visibility = View.GONE
            filtersEmptyView.visibility = View.VISIBLE
        } else {
            filtersRecyclerView.visibility = View.VISIBLE
            filtersEmptyView.visibility = View.GONE
        }
        (filtersRecyclerView.adapter as FilterAdapter).submitList(list)
    }

    data class Filter(
            var id: Long? = 0,
            var title: String = "",
            var type: Type? = Type.EQUAL_TO,
            var value: Any? = null
    ) {
        enum class Type {
            EXCLUDE, INCLUDE, MORE_THAN, LESS_THAN, EQUAL_TO
        }
    }

    class FilterAdapter(
            val removeFilterListener: (Filter) -> Unit
    ) : ListAdapter<Filter, BindableViewHolder>(
            SimpleDiffer<Filter> { it.id }
    ) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindableViewHolder {
            return ItemViewHolder(parent)
        }

        override fun onBindViewHolder(holder: BindableViewHolder, position: Int) {
            holder.bind()
        }

        inner class ItemViewHolder(parent: ViewGroup) : BindableViewHolder(parent, R.layout.filter_item) {

            override fun bind() {
                val item = getItem(adapterPosition)!!
                itemView as Chip
                itemView.text = item.title
                itemView.setOnCloseIconClickListener {
                    removeFilterListener.invoke(item)
                }
            }
        }
    }

    private class SimpleDiffer<T>(private val identifier: (T) -> Any?) : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
            return identifier.invoke(oldItem) == identifier.invoke(newItem)
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
            return oldItem == newItem
        }
    }

    open class BindableViewHolder(parent: ViewGroup, @LayoutRes layoutRes: Int) : RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
    ) {
        open fun bind() {
        }
    }
}