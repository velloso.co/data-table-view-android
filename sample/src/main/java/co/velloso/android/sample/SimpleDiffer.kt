package co.velloso.android.sample

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil

class SimpleDiffer<T>(private val identifier: (T) -> Any?) : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return identifier.invoke(oldItem) == identifier.invoke(newItem)
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return oldItem == newItem
    }
}