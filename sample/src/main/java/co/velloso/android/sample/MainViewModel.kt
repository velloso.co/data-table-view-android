package co.velloso.android.sample

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.toLiveData
import co.velloso.android.lifecycle.map
import co.velloso.android.lifecycle.switchMap

class MainViewModel : ViewModel() {

    val query = MutableLiveData<FoodFragment.FoodDataSource.Query>().apply {
        value = FoodFragment.FoodDataSource.Query()
    }

    val foods = query.switchMap {
        FoodFragment.FoodDataSource.Factory(query = it).toLiveData(pageSize = 5)
    }

    val filters = query.map {
        it.filters
    }
}