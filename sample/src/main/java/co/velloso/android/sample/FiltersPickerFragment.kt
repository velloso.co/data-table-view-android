package co.velloso.android.sample

import android.os.Bundle
import android.view.View
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.DialogFragment
import co.velloso.android.datatableview.FilterListView
import co.velloso.android.lifecycle.activityViewModel
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.filters_picker.view.*


class FiltersPickerFragment : DialogFragment(R.layout.filters_picker) {

    private val mainViewModel: MainViewModel by activityViewModel()

    private val typeFilters: List<FilterListView.Filter> by lazy {
        listOf(
                FilterListView.Filter(id = FoodFragment.Food.Type.FRUIT.ordinal.toLong(), title = getString(R.string.exclude).format(FoodFragment.Food.Type.FRUIT.name), type = FilterListView.Filter.Type.EXCLUDE, value = FoodFragment.Food.Type.FRUIT),
                FilterListView.Filter(id = FoodFragment.Food.Type.MEAT.ordinal.toLong(), title = getString(R.string.exclude).format(FoodFragment.Food.Type.MEAT.name), type = FilterListView.Filter.Type.EXCLUDE, value = FoodFragment.Food.Type.MEAT),
                FilterListView.Filter(id = FoodFragment.Food.Type.SUPPLEMENT.ordinal.toLong(), title = getString(R.string.exclude).format(FoodFragment.Food.Type.SUPPLEMENT.name), type = FilterListView.Filter.Type.EXCLUDE, value = FoodFragment.Food.Type.SUPPLEMENT)
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        // Toolbar
        view.toolbar.setNavigationIcon(R.drawable.ic_navigate_before)
        view.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        // Type filters
        typeFilters.forEach { filter ->
            val chip = Chip(ContextThemeWrapper(context, R.style.Widget_MaterialComponents_Chip_Filter))
            chip.text = filter.title
            chip.isCheckable = true
            chip.isChecked = false
            chip.setOnCheckedChangeListener { _, isChecked ->
                updateFilters(filter, isChecked)
            }
            view.filtersTypeChipGroup.addView(chip)
        }
    }

    private fun updateFilters(filter: FilterListView.Filter, isAdd: Boolean) {
        mainViewModel.query.value = mainViewModel.query.value?.apply {
            filters = mainViewModel.query.value!!.filters.toMutableList().apply {
                if (isAdd) add(filter)
                else remove(filter)
            }
        }
    }
}