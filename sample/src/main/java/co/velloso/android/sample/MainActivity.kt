package co.velloso.android.sample

import android.os.Bundle
import androidx.navigation.NavController
import co.velloso.android.statefulnavigation.StatefulNavController
import co.velloso.android.statefulnavigation.activity.NavHostActivity
import co.velloso.android.statefulnavigation.navigators.DialogFragmentNavigator
import co.velloso.android.statefulnavigation.navigators.StatefulFragmentNavigator


class MainActivity : NavHostActivity(R.navigation.navigation_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState) // need to be called so navController is created
        setContentView(R.layout.main)
    }

    override fun onCreateNavController(): NavController = StatefulNavController(this).apply {
        navigatorProvider.addNavigator(StatefulFragmentNavigator(this@MainActivity, supportFragmentManager, R.id.container))
        navigatorProvider.addNavigator(DialogFragmentNavigator(this@MainActivity, supportFragmentManager))
    }

    override fun onSupportNavigateUp() = navController.navigateUp()

    override fun onBackPressed() {
        when {
            navController.navigateUp() -> {
            }
            // Ensure the user always leaves from startDestination
            navController.currentDestination?.id != navController.graph.startDestination -> {
                navController.navigate(navController.graph.startDestination)
            }
            else -> finish()
        }
    }
}
