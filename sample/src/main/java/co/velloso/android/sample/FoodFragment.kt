package co.velloso.android.sample

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Checkable
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import co.velloso.android.datatableview.DataTableView
import co.velloso.android.datatableview.FilterListView
import co.velloso.android.lifecycle.activityViewModel
import co.velloso.android.statefulnavigation.extensions.activityNavController
import co.velloso.android.statefulrecyclerview.BindableViewHolder
import kotlinx.android.synthetic.main.food.view.*
import kotlinx.android.synthetic.main.food_item.view.*


class FoodFragment : Fragment(R.layout.food) {

    private val mainViewModel: MainViewModel by activityViewModel()

    private val navController by activityNavController()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        // Toolbar
        view.debounceSearchView.setOnDebouncedQueryListener { _, queryText, _ ->
            mainViewModel.query.value = mainViewModel.query.value?.apply {
                text = queryText.toString()
            }
        }
        view.tableToolbar.setOnMenuItemClickListener { item: MenuItem? ->
            when (item?.itemId) {
                R.id.action_clear_filters -> {
                    mainViewModel.query.value = mainViewModel.query.value?.apply {
                        filters = mainViewModel.query.value!!.filters.apply {
                            clear()
                        }
                    }
                    true
                }
                R.id.action_add_filter -> {
                    // Show filters picker
                    navController.navigate(R.id.filtersPickerFragment)

                    true
                }
                else -> false
            }
        }

        // Filters
        view.filterListView.onFilterRemoved = { filter ->
            mainViewModel.query.value = mainViewModel.query.value?.apply {
                // Cast to mutable list to create a new object and truely trigger the differ
                filters = mainViewModel.query.value!!.filters.toMutableList().apply {
                    remove(filter)
                }
            }
        }
        mainViewModel.filters.observe(viewLifecycleOwner, Observer {
            view.filterListView.submitList(it)
        })

        // Shared adapter
        val adapter = FoodAdapter()
        mainViewModel.foods.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

        // Table
        view.dataTableView?.columns = listOf(
                DataTableView.Column(0, getString(R.string.id), 1f, false, asc = false, value = R.string.id),
                DataTableView.Column(1, getString(R.string.name), 3f, true, asc = false, value = R.string.name),
                DataTableView.Column(2, getString(R.string.calories), 2f, true, asc = false, value = R.string.calories),
                DataTableView.Column(3, getString(R.string.protein), 2f, false),
                DataTableView.Column(4, getString(R.string.type), 2f, false)
        )
        view.dataTableView?.onCheckChanged = { isChecked ->
            view.dataTableView?.adapter?.currentList?.forEach {
                it.isChecked = isChecked
            }
            view.dataTableView?.adapter?.notifyDataSetChanged()
        }
        view.dataTableView?.onColumnSorted = { columns ->
            mainViewModel.query.value = mainViewModel.query.value?.apply {
                columnsSorted = columns.toMutableList()
            }
        }
        view.dataTableView?.adapter = adapter as PagedListAdapter<Checkable, *>

        // List
        view.foodListRecyclerView?.layoutManager = LinearLayoutManager(context)
        view.foodListRecyclerView?.adapter = adapter
    }

    class FoodAdapter(
            private val onSelected: ((Food) -> Unit)? = null
    ) : PagedListAdapter<Food, BindableViewHolder>(
            SimpleDiffer<Food> { it.id }
    ) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
                ItemViewHolder(parent)

        override fun onBindViewHolder(holder: BindableViewHolder, position: Int) {
            holder.bind()
        }

        inner class ItemViewHolder(parent: ViewGroup) : BindableViewHolder(parent, R.layout.food_item) {

            override fun bind() {
                val item = getItem(adapterPosition)!!
                itemView.checkboxButton?.isChecked = item.isChecked
                itemView.checkboxButton?.setOnCheckedChangeListener { _, isChecked ->
                    item.isChecked = isChecked
                }
                itemView.idTextView.text = item.id.toString()
                itemView.nameTextView.text = item.name
                itemView.caloriesTextView.text = item.calories.toString()
                itemView.proteinTextView.text = item.protein.toString()
                itemView.typeTextView.text = item.type.name
                itemView.setOnClickListener {
                    onSelected?.invoke(item)
                }
            }
        }
    }

    data class Food(
            var id: Long? = 0,
            var name: String = "",
            var calories: Int = 0,
            var protein: Int = 0,
            var sodium: Int = 0,
            var calcium: Int = 0,
            var type: Type = Type.OTHER

    ) : Checkable {

        private var isChecked: Boolean = false

        override fun setChecked(checked: Boolean) {
            isChecked = checked
        }

        override fun isChecked(): Boolean = isChecked

        override fun toggle() {
            isChecked = !isChecked
        }

        enum class Type {
            FRUIT, BEVERAGE, SUPPLEMENT, MEAT, DESERT, OTHER
        }
    }

    class FoodDataSource(
            private val query: Query
    ) : PageKeyedDataSource<Long, Food>() {

        private val queryRegex = "(?i)(${if (query.text.isNullOrBlank()) "." else query.text}*)"

        private val allFoods = listOf(
                Food(id = 0, name = "Whey Protein", calories = 10, protein = 6, sodium = 0, calcium = 0, type = Food.Type.SUPPLEMENT),
                Food(id = 1, name = "Banana", calories = 100, protein = 3, sodium = 2, calcium = 1, type = Food.Type.FRUIT),
                Food(id = 2, name = "Milk", calories = 80, protein = 6, sodium = 2, calcium = 100, type = Food.Type.BEVERAGE),
                Food(id = 3, name = "Apple", calories = 50, protein = 1, sodium = 2, calcium = 0, type = Food.Type.FRUIT),
                Food(id = 4, name = "Beef", calories = 10, protein = 6, sodium = 0, calcium = 0, type = Food.Type.MEAT),
                Food(id = 5, name = "Chicken", calories = 100, protein = 3, sodium = 2, calcium = 1, type = Food.Type.MEAT),
                Food(id = 6, name = "Chocolate", calories = 80, protein = 6, sodium = 2, calcium = 100, type = Food.Type.DESERT),
                Food(id = 7, name = "Water", calories = 0, protein = 0, sodium = 1, calcium = 0, type = Food.Type.BEVERAGE),
                Food(id = 8, name = "Whey Protein", calories = 10, protein = 6, sodium = 0, calcium = 0),
                Food(id = 9, name = "Banana", calories = 100, protein = 3, sodium = 2, calcium = 1),
                Food(id = 10, name = "Milk", calories = 80, protein = 6, sodium = 2, calcium = 100),
                Food(id = 11, name = "Apple", calories = 50, protein = 1, sodium = 2, calcium = 0),
                Food(id = 12, name = "Whey Protein", calories = 10, protein = 6, sodium = 0, calcium = 0),
                Food(id = 13, name = "Banana", calories = 100, protein = 3, sodium = 2, calcium = 1),
                Food(id = 14, name = "Milk", calories = 80, protein = 6, sodium = 2, calcium = 100),
                Food(id = 15, name = "Apple", calories = 50, protein = 1, sodium = 2, calcium = 0)
        )

        override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, Food>) {
            // unused
        }

        override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, Food>) {
            val items = allFoods
                    .filter { Regex(queryRegex).containsMatchIn(it.name) }
                    .filter { item ->
                        var result = true
                        query.filters.forEach { filter ->
                            if (filter.type == FilterListView.Filter.Type.EXCLUDE && filter.value == item.type) {
                                result = false
                                return@forEach
                            }
                            if (filter.type == FilterListView.Filter.Type.INCLUDE && filter.value != item.type) {
                                result = false
                                return@forEach
                            }
                        }
                        result
                    }
                    .let { filtered ->
                        when (query.columnsSorted.firstOrNull()?.value) {
                            R.string.name -> filtered.sortedBy { it.name }
                            R.string.calories -> filtered.sortedBy { it.calories }
                            else -> filtered
                        }
                    }
                    .take(params.requestedLoadSize)

            callback.onResult(
                    items,
                    null,
                    items.lastOrNull()?.id
            )
        }

        override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, Food>) {

            val items = allFoods
                    .filter { Regex(queryRegex).containsMatchIn(it.name) }
                    .filter { query.filters.find { filter -> filter.value == it.type } == null }
                    .let { filtered ->
                        when (query.columnsSorted.firstOrNull()?.value) {
                            R.string.name -> filtered.sortedBy { it.name }
                            R.string.calories -> filtered.sortedBy { it.calories }
                            else -> filtered
                        }
                    }
                    .filter { it.id ?: 0 > params.key }
                    .take(params.requestedLoadSize)

            callback.onResult(
                    items,
                    items.lastOrNull()?.id
            )
        }

        class Factory(
                private val query: Query
        ) : DataSource.Factory<Long, Food>() {
            override fun create() = FoodDataSource(query)
        }

        data class Query(
                var text: String? = null,
                var columnsSorted: MutableList<DataTableView.Column> = mutableListOf(),
                var filters: MutableList<FilterListView.Filter> = mutableListOf()
        )
    }
}